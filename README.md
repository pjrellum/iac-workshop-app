# Create a Docker Image in Azure Container Registry containing an API

## Steps

1. Deploy the (already built and published) application using the following command:
`az webapp deployment source config-zip -n <nameofyourappservice> -g rg-iacworkshop-app-dev --src out\app.zip`
1. Go to `<nameofyourappservice>.azurewebsites.net` and enjoy your fully working app.
