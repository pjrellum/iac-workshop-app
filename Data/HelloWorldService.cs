namespace iac_workshop_app.Data;

public class HelloWorldService
{
    private readonly HttpClient httpClient;

    public HelloWorldService(HttpClient httpClient)
    {
        this.httpClient = httpClient;
    }

    public async Task<string> Hi()
    {
        return await httpClient.GetAsync("HelloWorld/Hi").Result.Content.ReadAsStringAsync();
    }
}
