using System.Text.Json.Serialization;

namespace iac_workshop_app.Data;

public class Employee
{
    [JsonPropertyName("RowKey")]
    public string? Id { get; set; }

    public string? Name { get; set; }

    public string? Age { get; set; }

    public string? City { get; set; }

}
